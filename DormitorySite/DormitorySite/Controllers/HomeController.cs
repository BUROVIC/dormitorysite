﻿using DormitorySite.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DormitorySite.Controllers
{
    public class HomeController : Controller
    {
        DormitoryEntities db;
        public HomeController()
        {
            db = new DormitoryEntities();
            Globals.RoomsNums = db.Rooms.ToDictionary(r => r.ID, r => r.Floor.ToString() + r.Room_);
            Globals.RoomsNums[0] = "None";
        }
        
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Inventory()
        {
            return View(new InventoryModel(db.Inventories.ToList(), db.InventoryTypes.ToList()));
        }

        [HttpPost]
        public void AddNewItem(int typeId, int roomId)
        {
            db.Inventories.Add(new Inventory
            {
                TypeID = typeId,
                RoomID = roomId == 0 ? (int?)null : roomId
            });
            db.SaveChanges();
        }

        [HttpPost]
        public void RemoveInventoryItem(int id)
        {
            db.Inventories.Remove(db.Inventories.Find(id));
            db.SaveChanges();
        }

        [HttpPost]
        public void EditItemDescription(int id, string description)
        {
            db.Inventories.Find(id).Description = description;
            db.SaveChanges();
        }

        [HttpPost]
        public void ChangeItemRoom(int id, int roomId)
        {
            db.Inventories.Find(id).RoomID = roomId == 0 ? (int?)null : roomId;
            db.SaveChanges();
        }

        public ActionResult People()
        {
            return View(new PeopleModel(db.Persons.ToList(), db.Roles.ToList()));
        }

        [HttpPost]
        public void AddPerson(string name, int roleId, int roomId)
        {
            db.Persons.Add(new Person() {
                FirstName = name.Split(' ')[0],
                LastName = name.Split(' ')[1],
                RoleID = roleId,
                RoomID = roomId == 0 ? (int?)null : roomId
            });
            db.SaveChanges();
        }

        [HttpGet]
        public string GetDebtors()
        {
            return String.Join("\n", 
                db.Persons.ToList()
                    .Where(p => p.RoomID != null && (p.PaidUntil == null || p.PaidUntil < DateTime.Now))
                    .OrderBy(p => p.PaidUntil)
                    .Select(p => String.Format("{0} {1} - {2}", p.FirstName, p.LastName, 
                        (p.PaidUntil.HasValue ? p.PaidUntil.Value.ToString(Globals.DateFormat) : "*Newer*")))
                );
        }

        [HttpPost]
        public void RemovePerson(int id)
        {
            db.Persons.Remove(db.Persons.Find(id));
            db.SaveChanges();
        }

        public ActionResult Person(int id)
        {
            ViewBag.PersonId = id;
            return View(db.Persons.FirstOrDefault(p => p.ID == id));
        }

        [HttpPost]
        public void ChangePersonRoom(int id, int roomId)
        {
            db.Persons.Find(id).RoomID = roomId == 0 ? (int?)null : roomId;
            db.SaveChanges();
        }


        [HttpPost]
        public void ChangePaidUntil(int id, string newDate)
        {
            db.Persons.Find(id).PaidUntil = newDate == "" ? null :
                (DateTime?)DateTime.ParseExact(newDate, Globals.DateFormat, CultureInfo.InvariantCulture);
            db.SaveChanges();
        }

        public ActionResult Rooms()
        {
            return View(new RoomsModel(db.Rooms.ToList(), db.InternetProviders.ToList(), db.Inventories.ToList()));
        }

        [HttpPost]
        public void ChangeRoomProvider(int roomId, int providerId)
        {
            db.Rooms.Find(roomId).InternetProviderID = providerId;
            db.SaveChanges();
        }

        public ActionResult WorkShifts()
        {
            return View(new WorkShiftsModel(db.WorkShifts.ToList(), 
                db.Persons.Where(p => p.RoleID != 1).OrderBy(x => x.RoleID).ToList()));
        }

        [HttpPost]
        public void AddWorkShift(int employeeId, string startTime, string endTime)
        {
            db.WorkShifts.Add(new WorkShift() { EmployeeID = employeeId,
                StartTime = DateTime.ParseExact(startTime, Globals.DateTimeFormat, CultureInfo.InvariantCulture),
                EndTIme = DateTime.ParseExact(endTime, Globals.DateTimeFormat, CultureInfo.InvariantCulture)
            });
            db.SaveChanges();
        }

        [HttpPost]
        public void RemoveWorkShift(int id)
        {
            db.WorkShifts.Remove(db.WorkShifts.Find(id));
            db.SaveChanges();
        }


    }
}