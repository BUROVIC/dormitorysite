﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DormitorySite
{
    public static class Globals
    {
        public const string DateFormat = "dd.MM.yyyy";
        public const string DateTimeFormat = DateFormat + " HH:mm";

        //id, num
        public static Dictionary<int, string> RoomsNums;
    }
}