﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DormitorySite.Models
{
    public class InventoryModel
    {
        public List<Inventory> Inventory { get; private set; }
        public List<InventoryType> InventoryTypes { get; private set; }

        public InventoryModel(List<Inventory> inventory, List<InventoryType> inventoryTypes)
        {
            Inventory = inventory;
            InventoryTypes = inventoryTypes;
        }
    }
}