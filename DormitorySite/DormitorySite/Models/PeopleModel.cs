﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DormitorySite.Models
{
    public class PeopleModel
    {
        public List<Person> People { get; private set; }
        public List<Role> Roles { get; private set; }

        public PeopleModel(List<Person> people, List<Role> roles)
        {
            People = people;
            Roles = roles;
        }
    }
}