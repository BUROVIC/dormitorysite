﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DormitorySite.Models
{
    public class RoomsModel
    {
        public List<Room> Rooms { get; private set; }
        public List<InternetProvider> InternetProviders { get; private set; }
        public List<Inventory> Inventory { get; private set; }

        public RoomsModel(List<Room> rooms, List<InternetProvider> internetProviders, List<Inventory> inventory)
        {
            Rooms = rooms;
            InternetProviders = internetProviders;
            Inventory = inventory;
        }
    }
}