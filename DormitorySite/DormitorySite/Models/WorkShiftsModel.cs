﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DormitorySite.Models
{
    public class WorkShiftsModel
    {
        public List<WorkShift> WorkShifts { get; private set; }
        public List<Person> Employees { get; private set; }

        public WorkShiftsModel(List<WorkShift> workShifts, List<Person> employees)
        {
            WorkShifts = workShifts;
            Employees = employees;
        }
    }
}